export class Book {
  title:string = ''
  author:string = ''
  price?:number
  readonly id:number = 1

  constructor(author:string, title:string) {
    this.author = author
    if(title) {
      this.title = title
    }
  }
}

export class Video {
  private title?:string
  private author?:string
  private price?:number

  constructor(title:string, author?:string, price?:number){
    this.title = title
    this.author = author
    this.price = price
  }


  public toString():string {
    return `Video(title: ${this.title}, author: ${this.author}, price: ${this.price})`
  }


  public get getTitle() : string {
    if (this.title == null) {
      return "nope"
    }
    return this.title;
  }


}
