import { Component } from '@angular/core';
import { Book, Video } from 'src/model/Model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'exploring-typescript'
  readonly myNumber: number = 3.141592

  constructor(){
    // this.exploringArrays()
    // this.exploringLoops()
    let myCustomer:object = {'firstName': 'antoon', 'age': '34'}
    console.log(myCustomer)
    console.log(typeof myCustomer)

    let myBook = new Book('hi', 'hi')
    myBook.price = 1000000
    console.log(myBook);



    let myVideo = new Video("hi", "hi", 3.50)     // Video-class resides inside ../Book.ts
    console.log('myVideo => ' + myVideo)          // uses the toString()
    console.log('myVideo => ' , myVideo)          // appends the object

    const numbers = [1,2,3,4,5,6,7,8,9,10]
    const oddNumbers = numbers.filter( (number) => {
      return number % 2 === 1
    })
    const evenNumbers = numbers.filter( number => number % 2 === 0)
    console.log(evenNumbers)
    console.log(oddNumbers)


  }

  exploringLoops() {
    for (let i = 0; i < 20; i++) {
      console.log(i)
    }

    let myArray2:number[] = [1, 2, 3, 4, 5]
    for(const number of myArray2) {
      console.log(number)
    }

  }

  /** more like (java) lists */
  exploringArrays() {
    const myArray1 = new Array<number>(5);
    let myArray2:number[]
    myArray2 = [1, 2, 3]
    console.log(myArray1)      // length: 5
    console.log(myArray2)      // length: 3
    console.log(myArray2[2])   // returns element at index x

    console.log(myArray2.slice(1, 2))  // returns new array with element(s) from startIndex(=param1) 1 until endIndex(=param2) 2 (=excluded)
    console.log(myArray2.splice(1, 2)) // returns new array with x=param2 element(s) from index i=param1 => these records will be DELETED from source array
    console.log(myArray2)              // returns array with 1 element because we SPLICED the array previously

    myArray2.push(5) // appends element to the list
    myArray2.push(4)
    myArray2.push(3)
    console.log(myArray2)
    console.log(myArray2.pop())  // removes the last element of the list & returns its value
    console.log(myArray2)
    /** we can use push & pop as a LIFO queue */
  }


  someMethod() {
    const PI = 3.141592
    let anotherNumber:number

    anotherNumber = 2
    anotherNumber = 3

    /**does not compile*/
    // anotherNumber = 'hi'
    // this.myNumber = 12
    // PI = 4
  }

}
